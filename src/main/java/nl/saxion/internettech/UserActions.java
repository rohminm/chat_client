package nl.saxion.internettech;

public enum UserActions {
	HELO("#### Enter username to login:", null), //
	QUIT("Enter 1 to quit", null), //
	BCST("Enter 2 to broadcast message", new String[] { "Enter message to broadcast" }), //
	LIST_USERS("Enter 3 to list users", null), //
	LIST_GROUPS("Enter 4 to list groups of users", null), //
	CREATE_GROUP("Enter 5 to create group", new String[] { "Enter group name you want to create" }), //
	JOIN_GROUP("Enter 6 to join group", new String[] { "Enter group name to join" }), //
	STEP_OUT_OF_GROUP("Enter 7 to step from group", new String[] { "Enter group name to step out of" }), //
	SEND_MSG_TO_USER("Enter 8 to send message to another user", new String[] { "Enter user to deliver the message", "Enter message" }), //
	KICK_USER("Enter 9 to kick another user from the group", new String[] { "Enter group name", "Enter user to kick from the group" }), //
	SEND_FILE("Enter 10 to send file to another user", new String[] { "Enter file name", "Enter user to send file" }), //
	PING(null, null);

	private String actionPrompt;
	private String[] actionMessages;

	UserActions(String actionPrompt, String[] actionMessages) {
		this.actionPrompt = actionPrompt;
		this.actionMessages = actionMessages;
	}

	public String getActionPrompt() {
		return actionPrompt;
	}

	public String[] getActionMessages() {
		return actionMessages;
	}

}

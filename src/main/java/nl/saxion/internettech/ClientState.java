package nl.saxion.internettech;
enum ClientState {
		/**
		 * indicates that connection is not established yet
		 */
		CONNECTING,
		/**
		 * indicated that connection is establish (if connection is dropped
		 * client will be reconnected automatically)
		 */
		CONNECTED,
		/**
		 * indicates that clinet is not logged in yet
		 */
		LOGGING_IN,
		/**
		 * indicates that user is logged in (if connection is dropped client
		 * will be logged in again automatically)
		 */
		LOGGED_IN
	}
package nl.saxion.internettech;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Client {

	private String ip = "localhost";
	private int port = 1337;
	private int timeoutMillis = 2000;
	private boolean sslEnabled = false;

	private Socket socket;

	private BufferedReader reader;

	/*
	 * we use BufferedOutputStream instead of BufferedWriter because
	 * BufferedOutputStream throws exception
	 */
	private BufferedOutputStream bos;

	private String username;

	boolean loggedIn = false;
	/**
	 * Times to attempt to establish connection if server is down for example
	 */
	int maxTimesToRetry = 3;
	int connectionAttemptsCounter = 0;
	private boolean debug;
	private ProjectConfiguration conf;
	private ClientState state;

	TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub

		}

		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub

		}
	} };

	public Client(String ip, int port, int timeoutMillis, String username, boolean debug) {
		this.ip = ip;
		this.port = port;
		this.timeoutMillis = timeoutMillis;
		this.username = username;
		this.setDebug(debug);
	}

	public Client(String ip, int port, int timeoutMillis, boolean debug) {
		this.ip = ip;
		this.port = port;
		this.timeoutMillis = timeoutMillis;
		this.setDebug(debug);
	}

	public Client(String ip, int port, int timeoutMillis, String username) {
		this.ip = ip;
		this.port = port;
		this.timeoutMillis = timeoutMillis;
		this.username = username;
	}

	/**
	 * Connect to server on specified ip and port number. If server is down it
	 * tries to connect again specified number of time {@link #maxTimesToRetry}.
	 * Set socket timeout in order to handle dropped packets
	 */
	public void connect() {

		log("Connecting to server...");
		try {

			setState(ClientState.CONNECTING);

			// check if ssl is enabled
			if (sslEnabled) {
				SSLContext sc;
				try {

					// SSLContext protocols: TLS, SSL, SSLv3 etc
					sc = SSLContext.getInstance("TLSv1.2");
					sc.init(null, trustAllCerts, new SecureRandom());
					socket = sc.getSocketFactory().createSocket(ip, port);
					/*
					 * if ssl enabled SSLServerSocket instance is used to start
					 * SSL handshake
					 */
					((SSLSocket) socket).startHandshake();

				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} catch (KeyManagementException e) {
					e.printStackTrace();
				}
			} else {
				// when SSL NOT enabled
				socket = new Socket(ip, port);
			}
			/*
			 * Set timeout to the socket in order to handle dropped packets when
			 * read from input stream
			 * 
			 * @see socket.setSoTimeout(int timeout)
			 */
			socket.setSoTimeout(timeoutMillis);

			/*
			 * initialize writer and reader
			 */
			bos = new BufferedOutputStream(socket.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (ConnectException e) {
			/*
			 * Catching ConnectException in order to retry to connect again if
			 * server is down. Max. number of retry attempts defined in
			 * maxTimesToRetry
			 */
			connectionAttemptsCounter++;
			err("Error while connection setup (server may not be started)");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			if (connectionAttemptsCounter > maxTimesToRetry) {
				err("Maximum number of attempts to establish connection reached");
				System.exit(1);
			}
			connect();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Send message to the server. If connection resets (server drops
	 * connection) and client was logged in, client will be reconnected and
	 * logged in again automatically, if client was connected, but not logged in
	 * he will be connected automatically.
	 * 
	 * 
	 * @param protocol
	 *            - defined protocol keyword
	 * @param msg
	 *            - message to send to the server
	 */
	public void sendMessage(String protocol, String msg) {

		String message;
		if (msg == null) {
			message = protocol;
		} else {
			message = protocol + " " + msg;
		}

		log(message);
		try {
			// must append new line character
			message = message + "\n";

			synchronized (bos) {
				bos.write(message.getBytes());
				bos.flush();
			}

		} catch (IOException e) {
			err(e.getMessage());
			debug("Recovery");
			if (this.getState().equals(ClientState.LOGGED_IN)) {
				this.connect();
				this.login();
			} else {
				this.connect();
			}
		}

	}

	/**
	 * Calculate checksum using MD5 algorithm (checksum is calculated over file
	 * data)
	 * 
	 * @param dataToPass
	 *            - File data to send
	 * @return
	 */
	public String calculateChecksum(byte[] dataToPass) {
		MessageDigest md;

		byte[] data = new byte[dataToPass.length];

		for (int i = 0; i < dataToPass.length; i++) {
			data[i] = dataToPass[i];
		}

		try {
			md = MessageDigest.getInstance("MD5");
			md.update(data);

			byte[] mdbytes = md.digest();

			String checksum = new String();
			for (int i = 0; i < mdbytes.length; i++) {
				checksum += Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1);
			}

			log("Calculated checksum: " + checksum);

			return checksum;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * Sends file using specified protocol. Protocol contains of header and file
	 * data.
	 * First step is to send header in defined format <b>protocolName
	 * checksum:fileName:receiver:contentLength</b>, where protocol name is
	 * defined in advanced [SEND_FILE], fileName - name of the file user sends,
	 * receiver-user to send file, contentLenght- length of file data.
	 * Second step is to send actual data (data is encoded with Base64 encoded
	 * scheme)
	 * 
	 * @param protocol
	 * @param fileName
	 * @param user
	 */
	public void sendFile(String protocol, String fileName, String user) {

		try {
			Path path = Paths.get(fileName);
			byte[] data = Files.readAllBytes(path);

			// calculate check sum for given file
			String checksum = calculateChecksum(data);

			// check if checksum is calculated before sending the file
			if (checksum == null) {
				err("Checksum not calculated, try again...");
				return;
			}

			// encode file data
			String encodedStringData = Base64.getEncoder().encodeToString(data);
			log("Encoded file data: " + encodedStringData);

			byte[] encodedData = encodedStringData.getBytes(Charset.forName("UTF-8"));

			// get content length
			String contentLength = String.valueOf(encodedData.length);

			// format header
			String header = String.format("%s %s:%s:%s:%s\n", protocol, checksum, fileName, user, contentLength);

			// write protocol
			byte[] headerData = header.getBytes(Charset.forName("UTF-8"));
			bos.write(headerData);

			log("Sending [header,headerLength]: [" + header + ", " + headerData.length + "]");

			// write data
			bos.write(encodedData);
			bos.flush();
			log("Sending encodedData " + encodedData);

		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	/**
	 * Sets clients state to LOGGING_IN and calls {@link #sendMessage(String)}
	 * to login client.
	 */
	public void login() {

		setState(ClientState.LOGGING_IN);

		log("Logging in...");

		sendMessage(UserActions.HELO.toString(), username);
	}

	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void log(String msg) {
		System.out.println("[Client] " + msg);
	}

	private void err(String msg) {
		System.err.println("[Client] " + msg);
	}

	private void debug(String msg) {
		if (this.isDebug()) {
			log(msg);
		}
	}

	public boolean isLoggedIn() {

		return loggedIn;
	}

	public BufferedReader getReader() {
		return reader;
	}

	public void setReader(BufferedReader reader) {
		this.reader = reader;
	}

	public ClientState getState() {
		return state;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public void setState(ClientState state) {
		debug("Settings state to: " + state);
		this.state = state;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ProjectConfiguration getConf() {
		return conf;
	}

	public void setConf(ProjectConfiguration conf) {
		this.conf = conf;
	}

	public boolean isSslEnabled() {
		return sslEnabled;
	}

	public void setSslEnabled(boolean sslEnabled) {
		this.sslEnabled = sslEnabled;
	}

}

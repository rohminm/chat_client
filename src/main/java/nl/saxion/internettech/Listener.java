package nl.saxion.internettech;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.Base64;

/**
 * Client listens to server messages in separate thread
 * 
 *
 */
public class Listener extends Thread {

	private final Client client;

	private static String FILE_DOWNLOAD_LOCATION = "/tmp/downloads/";// can be
																		// changed
	private static String USERNAME_FORMAT_ERR = "-ERR username has an invalid format (only characters, numbers and underscores are allowed)";
	private static String ALREADY_LOGGED_IN_ERR = "-ERR user already logged in";

	public Listener(Client client) {
		this.client = client;
	}

	@Override
	public void run() {
		while (true) {
			try {
				String response = readLineFrom(client.getSocket().getInputStream());
				if (response != null) {
					log("Server msg: " + response);

					processResponse(response);
				}
				/*
				 * HANDLE DROPPED PACKETS (timeout happens due to client timeout
				 * that is set. client.getReader().readLine() throws
				 * SocketTimeoutException if timeout is set to the socket )
				 */
			} catch (SocketTimeoutException e) {
				debug("Waiting for server to send data...");
				recoverIfNeeded();

				continue;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Checks clients state in order to specify recovery method. If client was
	 * trying to connect than try to connect automatically , if client was
	 * trying to log in than try to log in again automatically. </br>
	 * </br>
	 * <b> @Note: This method assumes drop packets, not dropped connection. So
	 * if you are trying to login and server drops packets you don't need to
	 * connect again in order to log in, just try logging in again.</b>
	 */
	private void recoverIfNeeded() {
		if (client.getState().equals(ClientState.CONNECTING)) {
			debug("Recover connection");
			client.connect();
		} else if (client.getState().equals(ClientState.LOGGING_IN)) {
			debug("recovery login");
			client.login();
		}

	}

	/**
	 * Process server response in order to set appropriate client state.
	 * 
	 * @param response
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	private void processResponse(String response) throws NumberFormatException, IOException {
		// server returns +OK
		if (response.equals("+OK Goodbye")) {
			log("Disconneting from server...");
			System.exit(1);
		}
    if (response.equals("+OK PING")){
		  System.out.print("GOT a PING SENDING PONG");
      client.sendMessage("PONG", "");
    }

		if (client.getState().equals(ClientState.CONNECTING)) {

			if (response.equals(UserActions.HELO.toString() + " " + "Welkom to WhatsUpp!")) {
				client.setState(ClientState.CONNECTED);
				log("Connected successfully");
				return;
			} else {
				client.connect();
				log("Data corrupted, recover connection");
			}

		}

		else if (client.getState().equals(ClientState.LOGGING_IN)) {
			// server returns +OK
			if (response.equals("+OK " + client.getUsername())) {
				client.setState(ClientState.LOGGED_IN);
				if (client.getConf().equals(ProjectConfiguration.LEVEL1)) {
					log("Done! Change project configuration to LEVEL 2 in order to continue communication");
					System.exit(1);
				}
			} else {
				client.login();
				log("Data corrupted, recover login");
			}
			return;
		}

		if (response.startsWith(UserActions.SEND_FILE.toString())) {

			log("Receiving file...");

			/*
			 * read header data which is sent in format SEND_FILE
			 * checksum:fileName:sendFrom:contentLength
			 */
			String[] payloadData = response.split(":");
			if (payloadData.length < 4) {
				err("Data in header not ok, try again...");
			}

			String protocolAndChecksum = payloadData[0];
			// get checksum
			String checkSum = protocolAndChecksum.split(" ")[1];
			// file absolute path is sent within fileName
			String path[] = payloadData[1].split("/");
			// get file name from absoulte path
			String fileName = path[path.length - 1];
			// get sender username
			String sentFrom = payloadData[2];
			// get file content length
			String contentLength = payloadData[3];

			System.out.print("[Listener] Received payload ");
			for (int i = 0; i < payloadData.length; i++) {
				System.out.print("[" + i + "] " + payloadData[i] + ", ");
			}
			System.out.println();
			// read file data
			String data = readAllFrom(client.getSocket().getInputStream(), Integer.parseInt(contentLength));
			log("Received encoded data: [" + data + "]");

			saveInputToFile(data, FILE_DOWNLOAD_LOCATION + fileName, checkSum);
		}
	}

	InputStreamReader reader;

	private String readLineFrom(InputStream stream) throws IOException {
		if (reader == null) {
			reader = new InputStreamReader(stream);
		}
		StringBuffer buffer = new StringBuffer();

		for (int character = reader.read(); character != -1; character = reader.read()) {
			if (character == '\n')
				break;
			buffer.append((char) character);
		}

		if (buffer.toString().isEmpty()) {
			return null;
		}
		return buffer.toString();
	}

	private String readAllFrom(InputStream stream, int length) throws IOException {

		char[] data = new char[length];
		reader.read(data);

		return new String(data);
	}

	private void saveInputToFile(String base64data, String fileName, String checkSum) {

		byte[] data = new byte[1024];

		try {

			data = Base64.getDecoder().decode(base64data);

			/*
			 * check if checksum calculated from client who sends the file
			 * matches calculated checksum for received file, if not file will
			 * not be saved
			 */
			String calculatedCheckSum = client.calculateChecksum(data);
			if (calculatedCheckSum != null && !calculatedCheckSum.equals(checkSum)) {
				log("Checksums don't match! Try again...");
				return;
			} else {
				log("Checksums matches");
			}

			FileOutputStream stream = null;
			try {
				stream = new FileOutputStream(fileName);
				stream.write(data);
				stream.close();
				log("Path to file: " + fileName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	private void log(String msg) {
		System.out.println("[Listener] " + msg);
	}

	private void err(String msg) {
		System.err.println("[Listener] " + msg);
	}

	private void debug(String msg) {
		if (client.isDebug()) {
			log(msg);
		}
	}
}

package nl.saxion.internettech;

import java.util.Scanner;

public class ClientMain {

	private static final String IP = "localhost";
	private static final int PORT = 1337;
	private static final int SSL_PORT = 1443;
	private static final int TIMEOUT_MILLIS = 12000;
	private static final ProjectConfiguration cf = ProjectConfiguration.LEVEL_2_AND_3;

	public static void main(String[] args) {

		// enable log debugging
		boolean debug = false;

		Scanner sc = new Scanner(System.in);
		Boolean enableSSL = null;

		// wait for user to choose if data should be encrypted
		do {
			System.out.println("Enable SSL (yes/no)?");
			String userInput = sc.nextLine();
			if (userInput != null && userInput.equalsIgnoreCase("yes")) {
				enableSSL = true;
				System.out.println("SSL enabled!");
			} else if (userInput != null && userInput.equalsIgnoreCase("no")) {
				enableSSL = false;
				System.out.println("SSL not enabled!");

			}
		} while (enableSSL == null);

		Client client = null;
		if (enableSSL) {
			client = new Client(IP, SSL_PORT, TIMEOUT_MILLIS, debug);
			client.setSslEnabled(true);
		} else {
			client = new Client(IP, PORT, TIMEOUT_MILLIS, debug);

		}

		// set Project Configuration - choose between level1 and level 2 and 3
		client.setConf(cf);

		// connect to server
		client.connect();

		Listener listener = new Listener(client);
		listener.start();

		while (!client.getState().equals(ClientState.LOGGED_IN)) {
			try {

				Thread.sleep(2000);

				/*
				 * if client doesn't establish connection to the server yet
				 * (client state keeps CONNECTING, not CONNECTED) skip login.
				 * This is necessary because client can't see login prompt until
				 * he is connected.
				 */
				if (client.getState().equals(ClientState.CONNECTING)) {
					continue;
				}
				/*
				 * wait for server to respond that client has successfully
				 * logged in to break this loop
				 */
				if (client.getState().equals(ClientState.LOGGED_IN)) {
					System.out.println("You are logged in!");
					break;
				}

				System.out.println(UserActions.HELO.getActionPrompt());

				System.out.println();

				String input = null;
				input = sc.nextLine();

				client.setUsername(input);
				client.login();

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		/**
		 * After client has been logged in, he can continue communication and
		 * choose some actions
		 */

		while (true) {
			System.out.println("#### Choose action:");
			System.out.println(UserActions.QUIT.getActionPrompt());
			System.out.println(UserActions.BCST.getActionPrompt());
			System.out.println(UserActions.LIST_USERS.getActionPrompt());
			System.out.println(UserActions.LIST_GROUPS.getActionPrompt());
			System.out.println(UserActions.CREATE_GROUP.getActionPrompt());
			System.out.println(UserActions.JOIN_GROUP.getActionPrompt());
			System.out.println(UserActions.STEP_OUT_OF_GROUP.getActionPrompt());
			System.out.println(UserActions.SEND_MSG_TO_USER.getActionPrompt());
			System.out.println(UserActions.KICK_USER.getActionPrompt());
			System.out.println(UserActions.SEND_FILE.getActionPrompt());

			System.out.println();

			String action = sc.nextLine();

			/*
			 * this is used to check if user entered commmand from defined list
			 * of commands, if not he will be prompt --Command should be in
			 * specified values--
			 */
			boolean verifyCommand = false;

			for (UserActions ua : UserActions.values()) {
				// if user just clicked enter than prompt actions to him again
				if (action == null || action.isEmpty()) {
					System.err.println("Value not valid, please try again...");
					break;
				}

				/*
				 * check if user typed action that is equal to one of the
				 * possible actions (action defined in UserActions)
				 * 
				 * @Note: ordinal is position of Enum defined in UserAction
				 * (like serial number), it starts from ZERO
				 * 
				 */
				if (action.equals(String.valueOf((ua.ordinal())))) {
					verifyCommand = true;

					if (action.equals("10")) {

						System.out.println("Enter file name: ");
						String fileName = sc.nextLine();
						System.out.println("Enter receiver: ");
						String receiver = sc.nextLine();
						client.sendFile(UserActions.SEND_FILE.toString(), fileName, receiver);
						break;

					}

					if (ua.getActionMessages() == null) {
						/*
						 * if action messages is null than send only protocol,
						 * example: to list users you send only LIST_USERS no
						 * need for further messages, but for example to add
						 * user to group you must specify username to add and
						 * group name
						 */
						if (debug) {
							System.out.println("Sending only protocol");
						}
						client.sendMessage(ua.name(), null);
					} else {
						/*
						 * if action messages exits wait for user input and send
						 * protocol and messages
						 */
						StringBuilder sb = new StringBuilder();
						String data = null;
						for (int i = 0; i < ua.getActionMessages().length; i++) {
							System.out.println(ua.getActionMessages()[i]);
							data = sc.nextLine();
							sb.append(data);
							if (i == ua.getActionMessages().length - 1) {
								break;
							} else {
								/*
								 * this delimiter is used to send messages -
								 * defined as a protocol, so that server knows
								 * in which format data is sent and how to read
								 * it
								 */
								sb.append(":");
							}
						}

						client.sendMessage(ua.name(), sb.toString());

					}

				}
			}

			if (!verifyCommand) {
				System.err.println("Command should be in specified values");
			}

			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

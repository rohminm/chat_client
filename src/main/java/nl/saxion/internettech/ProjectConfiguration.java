package nl.saxion.internettech;

public enum ProjectConfiguration {

	/**
	 * After connection has been established and user is successfully logged in
	 * application exits
	 */
	LEVEL1, //
	/**
	 * User can choose to have encrypted communication. (level 3)
	 * After connection has been established and user is successfully logged in,
	 * user can choose action such as: create groups, add user to group, join
	 * group etc... (level 2).
	 * Possible actions include sending file to another user. (level 3)
	 * 
	 */
	LEVEL_2_AND_3 //

}
